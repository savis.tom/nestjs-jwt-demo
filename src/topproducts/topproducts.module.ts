import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { TopProductsController } from './topproducts.controller';
import { TopProductsService } from './topproducts.service';
import { TopProductSchema } from './topproduct.model';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'TopProduct', schema: TopProductSchema }]),
    ],
    controllers: [TopProductsController],
    providers: [TopProductsService],
})
export class TopProductsModule { }
