import * as mongoose from 'mongoose';

export const TopProductSchema = new mongoose.Schema({
    name: { type: String, required: true, min: 4, max: 30 },
    brand: { type: String, required: true, min: 4, max: 30 },
    descProduct: { type: String, required: true, max: 500 },
    OldPrice: { type: Number, required: true },
    NewPrice: { type: Number, required: true },
    ColorProduct: { type: Array, default: [] },
    ImgUrlProduct: { type: Object, default: {} },
});

export interface TopProduct extends mongoose.Document {
    id: string;
    name: string;
    brand: string;
    descProduct: string;
    OldPrice: number;
    NewPrice: number;
    ColorProduct: string[];
    ImgUrlProduct: object;
}
