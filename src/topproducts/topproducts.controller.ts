import {
    Controller,
    Post,
    Body,
    Get,
    Param,
    Patch,
    Delete,
    Put,
} from '@nestjs/common';

import { TopProductsService } from './topproducts.service';

@Controller('/api/TopProduct')
export class TopProductsController {
    constructor(private readonly productsService: TopProductsService) { }

    @Post()
    async addProduct(
        @Body('name') prodName: string,
        @Body('brand') prodBrand: string,
        @Body('descProduct') prodDescProduct: string,
        @Body('OldPrice') prodOldPrice: number,
        @Body('NewPrice') prodNewPrice: number,
        @Body('ColorProduct') prodColorProduct: string[],
        @Body('ImgUrlProduct') prodImgUrlProduct: {},
    ) {
        const generatedId = await this.productsService.insertProduct(
            prodName,
            prodBrand,
            prodDescProduct,
            prodOldPrice,
            prodNewPrice,
            prodColorProduct,
            prodImgUrlProduct,
        );
        return { id: generatedId };
    }

    @Get()
    async getAllProducts() {
        const products = await this.productsService.getProducts();
        return products;
    }

    @Get(':id')
    getProduct(@Param('id') prodId: string) {
        return this.productsService.getSingleProduct(prodId);
    }

    @Patch(':id')
    async updateProduct(
        @Param('id') prodId: string,
        @Body('name') prodName: string,
        @Body('brand') prodBrand: string,
        @Body('descProduct') prodDescProduct: string,
        @Body('OldPrice') prodOldPrice: number,
        @Body('NewPrice') prodNewPrice: number,
        @Body('ColorProduct') prodColorProduct: string[],
        @Body('ImgUrlProduct') prodImgUrlProduct: {},
    ) {
        await this.productsService.updateProduct(prodId,
            prodName,
            prodBrand,
            prodDescProduct,
            prodOldPrice,
            prodNewPrice,
            prodColorProduct,
            prodImgUrlProduct
        );
        return "Complete Updated Product";
    }

    @Delete(':id')
    async removeProduct(@Param('id') prodId: string) {
        await this.productsService.deleteProduct(prodId);
        return null;
    }
}
