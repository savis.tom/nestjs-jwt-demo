import { Test } from '@nestjs/testing';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { TopProduct } from './topproduct.model';

@Injectable()
export class TopProductsService {
    constructor(
        @InjectModel('TopProduct') private readonly productModel: Model<TopProduct>,
    ) { }

    async insertProduct(name: string, brand: string, descProduct: string, OldPrice: number, NewPrice: number, ColorProduct: object, ImgUrlProduct: object) {
        const newProduct = new this.productModel({
            name: name, brand: brand, descProduct: descProduct, OldPrice: OldPrice, NewPrice: NewPrice, ColorProduct: ColorProduct, ImgUrlProduct: ImgUrlProduct
        });
        const result = await newProduct.save();
        return result.id as string;
    }

    async getProducts() {
        const products = await this.productModel.find().exec();
        return products.map(prod => ({
            id: prod.id,
            name: prod.name,
            brand: prod.brand,
            descProduct: prod.descProduct,
            OldPrice: prod.OldPrice,
            NewPrice: prod.NewPrice,
            ColorProduct: prod.ColorProduct,
            ImgUrlProduct: prod.ImgUrlProduct,
        }));
    }

    async getSingleProduct(productId: string) {
        const product = await this.findProduct(productId);
        return {
            id: product.id,
            name: product.name,
            brand: product.brand,
            descProduct: product.descProduct,
            OldPrice: product.OldPrice,
            NewPrice: product.NewPrice,
            ColorProduct: product.ColorProduct,
            ImgUrlProduct: product.ImgUrlProduct,
        };
    }

    async updateProduct(

        productId: string,
        name: string,
        brand: string,
        descProduct: string, OldPrice: number, NewPrice: number, ColorProductUpdate: string[], ImgUrlProduct: object
    ) {
        const product = await this.findProduct(productId);
        const ColorProduct = product.ColorProduct;
        const updatedProduct = await this.findProduct(productId);
        if (name) {
            updatedProduct.name = name;
        }
        if (brand) {
            updatedProduct.brand = brand;
        }
        if (descProduct) {
            updatedProduct.descProduct = descProduct;
        }
        if (OldPrice) {
            updatedProduct.OldPrice = OldPrice;
        }
        if (NewPrice) {
            updatedProduct.NewPrice = NewPrice;
        }
        if (ColorProductUpdate) {
            const AssignColor = [...ColorProduct, ...ColorProductUpdate];
            const UpdateDataColor = Array.from(new Set(AssignColor));
            // console.log(UpdateDataColor);
            updatedProduct.ColorProduct = UpdateDataColor;
        }
        if (ImgUrlProduct) {
            updatedProduct.ImgUrlProduct = ImgUrlProduct;
        }
        updatedProduct.save();
    }

    async deleteProduct(prodId: string) {
        const result = await this.productModel.deleteOne({ _id: prodId }).exec();
        if (!result) {
            throw new NotFoundException('Could not find product.');
        }
    }

    private async findProduct(id: string): Promise<TopProduct> {
        let product;
        try {
            product = await this.productModel.findById(id).exec();
        } catch (error) {
            throw new NotFoundException('Could not find product.');
        }
        if (!product) {
            throw new NotFoundException('Could not find product.');
        }
        return product;
    }
}
