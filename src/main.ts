import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';

import { AppModule } from './app.module';
// import 'dotenv/config'
import * as config from 'config';
import * as express from 'express';


async function bootstrap() {

  // set up cors cho server tranhs truong hop truy cap yeu cau Cors


  const app = await NestFactory.create(AppModule);

  // app.enableCors(
  //   {
  //     origin: ['http://localhost:3000', 'http://localhost:9000', "*"],
  //     methods: "GET,HEAD,POST,PUT,DELETE,PATCH",
  //     allowedHeaders: "Content-Type, Accept",
  //     credentials: true,
  //   }
  // );
  // const serverConfig = config.get("server")
  app.useGlobalPipes(new ValidationPipe());

  if (process.env.NODE_ENV === "development") {

    app.enableCors(
      {
        origin: "*",
        methods: "GET,HEAD,POST,PUT,DELETE,PATCH",
        allowedHeaders: "Content-Type, Accept",
        credentials: true,
      }
    );
  } else {

    // app.enableCors({
    //   origin: "*",
    //   methods: "GET,HEAD,POST,PUT,DELETE,PATCH",
    //   allowedHeaders: "Content-Type, Accept",
    // credentials: true,
    // })
    app.enableCors({ origin: process.env.Origin });
    // console.log("Accepting Orgin: ");
  }

  const PORT = process.env.PORT || 9000

  app.use(express.json());

  await app.listen(PORT, () => {
    console.log("Start Listening with Port:", PORT);
  });
}

bootstrap();
