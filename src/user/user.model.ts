import * as mongoose from 'mongoose'

export const UserSchema = new mongoose.Schema({
    displayName: {
        type: String,
        require: true,
        min: 3,
        max: 20,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        max: 50,
        unique: true,
    },
    password: {
        type: String,
        required: true,
        min: 6,
    },
    photoURL: {
        type: String,
        default: "",
    },
    coverPhotoURL: {
        type: String,
        default: "",
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
    desc: {
        type: String,
        max: 50,
    },
    city: {
        type: String,
        max: 50,
    },
    from: {
        type: String,
        max: 50,
    },
    phoneNumber: {
        type: String,
        min: 9,
        max: 11,
        default: "+840000000"
    }
},
    { timestamps: true }
);

export interface User extends mongoose.Document {
    id: string;
    displayName: string;
    email: string;
    password: string;
    photoURL: string;
    coverPhotoURL: string;
    isAdmin: boolean;
    desc: string;
    city: string;
    from: string;
    phoneNumber: string;
}

