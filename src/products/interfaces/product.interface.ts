import { Document } from 'mongoose'

export interface IProduct extends Document {
    readonly id?: string;
    readonly name?: string;
    readonly brand?: string;
    readonly descProduct?: string;
    readonly OldPrice?: number;
    readonly NewPrice?: number;
    readonly ColorProduct?: string[];
    readonly ImgUrlProduct?: object;
    readonly DetailProduct?: object;
    readonly QuantityProductAndSize?: object;
}