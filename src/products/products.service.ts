import { Test } from '@nestjs/testing';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Product } from './product.model';
import { QueryOptions } from './query-options.config';

import { IProduct } from './interfaces/product.interface'

import { ProductDto } from './dto/product.dto'


@Injectable()
export class ProductsService {
  constructor(
    @InjectModel('Product') private readonly productModel: Model<Product>,
  ) { }

  // PostMan

  async insertProduct(name: string, brand: string, descProduct: string, OldPrice: number, NewPrice: number, ColorProduct: object, ImgUrlProduct: object, DetailProduct: object, QuantityProductAndSize: object) {
    const newProduct = new this.productModel({
      name: name, brand: brand, descProduct: descProduct, OldPrice: OldPrice, NewPrice: NewPrice, ColorProduct: ColorProduct, ImgUrlProduct: ImgUrlProduct, DetailProduct: DetailProduct, QuantityProductAndSize: QuantityProductAndSize
    });
    const result = await newProduct.save();
    return result.id as string;
  }

  async getProducts(options: QueryOptions, res) {

    // Getting the set Headers
    // const headers = res.getHeaders();

    // // Printing those headers
    // console.log(headers);

    // const totalCountHeader = headers['x-total-count'];


    // console.log(headers, totalCountHeader);

    const _page = Number.parseInt(options._page)
    const _limit = Number.parseInt(options._limit);
    const _skip = (_page - 1) * _limit;
    const _totalRow = await this.productModel.count();
    // 
    const products = await this.productModel.find().limit(Number(_limit)).skip(_skip).exec();

    const InfoProduct = {
      data: {
        product: products.map(prod => ({
          id: prod.id,
          name: prod.name,
          brand: prod.brand,
          descProduct: prod.descProduct,
          OldPrice: prod.OldPrice,
          NewPrice: prod.NewPrice,
          ColorProduct: prod.ColorProduct,
          ImgUrlProduct: prod.ImgUrlProduct,
          DetailProduct: prod.DetailProduct,
          QuantityProductAndSize: prod.QuantityProductAndSize,
        })),
        pagination: {
          _page: _page,
          _skip: _skip,
          _limit: _limit,
          _totalRow: _totalRow,
        },
      }
    }

    return res.jsonp(
      InfoProduct
    )
  }

  async getAllProducts() {

    const productRepository = this.productModel;

    const products = await productRepository.find()

    return products.map(prod => ({
      id: prod.id,
      name: prod.name,
      brand: prod.brand,
      descProduct: prod.descProduct,
      OldPrice: prod.OldPrice,
      NewPrice: prod.NewPrice,
      ColorProduct: prod.ColorProduct,
      ImgUrlProduct: prod.ImgUrlProduct,
      DetailProduct: prod.DetailProduct,
      QuantityProductAndSize: prod.QuantityProductAndSize,
    }));
  }

  async getSingleProduct(productId: string) {
    const product = await this.findProduct(productId);
    return {
      id: product.id,
      name: product.name,
      brand: product.brand,
      descProduct: product.descProduct,
      OldPrice: product.OldPrice,
      NewPrice: product.NewPrice,
      ColorProduct: product.ColorProduct,
      ImgUrlProduct: product.ImgUrlProduct,
      DetailProduct: product.DetailProduct,
      QuantityProductAndSize: product.QuantityProductAndSize,
    };
  }

  async updateProduct(

    productId: string,
    name: string,
    brand: string,
    descProduct: string,
    OldPrice: number,
    NewPrice: number,
    ColorProductUpdate: string[],
    ImgUrlProduct: object,
    DetailProduct: object,
    QuantityProductAndSize: any[],
  ) {
    const product = await this.findProduct(productId);
    const ColorProduct = product.ColorProduct;
    const updatedProduct = await this.findProduct(productId);
    if (name) {
      updatedProduct.name = name;
    }
    if (brand) {
      updatedProduct.brand = brand;
    }
    if (descProduct) {
      updatedProduct.descProduct = descProduct;
    }
    if (OldPrice) {
      updatedProduct.OldPrice = OldPrice;
    }
    if (NewPrice) {
      updatedProduct.NewPrice = NewPrice;
    }
    if (ColorProductUpdate) {
      const AssignColor = [...ColorProduct, ...ColorProductUpdate];
      const UpdateDataColor = Array.from(new Set(AssignColor));
      // console.log(UpdateDataColor);
      updatedProduct.ColorProduct = UpdateDataColor;
    }
    if (ImgUrlProduct) {

      const updateImgUrlProduct = { ...ImgUrlProduct }
      updatedProduct.ImgUrlProduct = updateImgUrlProduct;
    }
    if (DetailProduct) {
      updatedProduct.DetailProduct = Object.assign(DetailProduct);
    }
    if (QuantityProductAndSize) {
      updatedProduct.QuantityProductAndSize = QuantityProductAndSize;
    }
    updatedProduct.save();
  }

  async deleteProduct(prodId: string, res) {
    const result = await this.productModel.deleteOne({ _id: prodId }).exec();
    // console.log(result.deletedCount)
    if (result.deletedCount === 1) {
      res.status(200).jsonp("Data Product has been deleted");
    }
    if (result.deletedCount === 0) {
      throw new NotFoundException('Could not find product.');
    }
  }

  private async findProduct(id: string): Promise<Product> {
    let product;
    try {
      product = await this.productModel.findById(id).exec();
    } catch (error) {
      throw new NotFoundException('Could not find product.');
    }
    if (!product) {
      throw new NotFoundException('Could not find product.');
    }
    return product;
  }

  // Graphql

  async createProductGraphql(createProductGraphql: ProductDto): Promise<IProduct> {
    const createProduct = new this.productModel(createProductGraphql);
    return await createProduct.save();
  }

  async findAllGraphql(): Promise<IProduct[]> {
    return await this.productModel.find().exec();
  }

  async findByIdGraphql(id: string): Promise<IProduct> {
    return await this.productModel.findById(id).exec();
  }

  async findIdAndUpdate(id: string, updateProduct?: ProductDto): Promise<IProduct> {
    const product = await this.findProduct(id);
    const ColorProduct = product.ColorProduct;
    const QuantityProductAndSize = product.QuantityProductAndSize;
    const RateStart: any = product.DetailProduct

    if (updateProduct.name) {
      product.name = updateProduct.name;
    }

    if (updateProduct.brand) {
      product.brand = updateProduct.brand;
    }

    if (updateProduct.descProduct) {
      product.descProduct = updateProduct.descProduct;
    }

    if (updateProduct.OldPrice) {
      product.OldPrice = updateProduct.OldPrice;
    }

    if (updateProduct.NewPrice) {
      product.NewPrice = updateProduct.NewPrice;
    }

    if (updateProduct.ColorProduct) {
      const AssignColor = [...ColorProduct, ...updateProduct.ColorProduct];
      const UpdateDataColor = Array.from(new Set(AssignColor));
      // console.log(updateProduct.ColorProduct);
      product.ColorProduct = UpdateDataColor;
    }

    if (updateProduct.ImgUrlProduct) {
      const updateImgUrlProduct = { ...updateProduct.ImgUrlProduct }
      product.ImgUrlProduct = updateImgUrlProduct;
    }

    if (updateProduct.DetailProduct) {
      // product.DetailProduct = updateProduct.DetailProduct.rate;
      // console.log(RateStart.rate);

      const updateRateData: any = updateProduct.DetailProduct.rate

      const dataNew = [...RateStart.rate, ...updateRateData];

      const dataUpdate = {
        rate: dataNew.sort()
      }

      product.DetailProduct = dataUpdate;

      // console.log(dataUpdate);

    }
    if (updateProduct.QuantityProductAndSize) {

      const data = QuantityProductAndSize

      let dataValue: any = []
      const dataArray: any = []

      for (var i = 0; i < updateProduct.QuantityProductAndSize.length; i++) {
        dataArray.push({ ...updateProduct.QuantityProductAndSize[i] })
        dataValue = [...data[Symbol.iterator]()];
      }

      var arr1 = dataValue;
      var arr2 = dataArray;

      var arr = arr1.concat(arr2).reduce((prev, current, index, array) => {

        // console.log(index)

        // khoi tao prev result: [], keys: {} ==> result [ { size: 'M', QuantityProduct: 10 }]
        // ==> keys == "M"
        // sau do prev [ { size: 'M', QuantityProduct: 10 },{ size: 'L', QuantityProduct: 10 }] ==> keys = 'M' , "L"

        // final
        //  result: [
        //  { size: 'M', QuantityProduct: 10 },
        //  { size: 'L', QuantityProduct: 2 },
        //  { size: 'XL', QuantityProduct: 1 },
        //  { size: '2XL', QuantityProduct: 5 }
        //  ],
        // keys: { M: 0, L: 1, XL: 2, '2XL': 3 }

        // current
        //  { size: 'M', QuantityProduct: 10 }
        //  { size: 'L', QuantityProduct: 2 }
        //  { size: 'XL', QuantityProduct: 1 }
        //  { size: '2XL', QuantityProduct: 5 }
        //  { size: 'M', QuantityProduct: 10 }

        // index = length result + length keys 

        if (!(current.size in prev.keys)) {
          // console.log(prev.keys[current.size] = index)
          prev.keys[current.size] = index; // gan lai index
          prev.result.push(current);
          // push du lieu vao mang result
        }
        else {
          // neu co roi toi set lai gia tri
          prev.result[prev.keys[current.size]] = current;
        }

        // console.log(prev.result[prev.keys[current.size]] = current)

        // if current.size !== prev.keys=(M) ==> push vao array
        // console.log(prev.keys, current.size, current)

        return prev;
      }, { result: [], keys: {} }).result;

      // console.log(arr)

      // const data = 

      // console.log(dataUpdate.sort())

      product.QuantityProductAndSize = arr

    }
    return product.save();
  }

  async deleteProductGraphql(id: string) {
    const result = await this.productModel.deleteOne({ _id: id }).exec();
    console.log(result)
    if (result) {
      return "Data Product has been deleted";
    }


    if (!result) {
      throw new NotFoundException('Could not find product.');
    }
  }

}
